# hardware-test

- 1) - Prototype 1,
    - Check the shcematic PDF and related datasheet, solder the following 5 components to demonstrate a 2.5V DC-DC power unit.
    - Test it with a multi-meter and an ociliscope for the input/output voltage. Make a note on the A4 paper, take a wave screenshot by your cellphone to show the result. 

- 2) - Prototype 2,
    - Check the schematic PDF and related datasheet, quick prototype a temperature measure system and display it on a 4-digi display, refresh it every 1s.
    - Publish your code on your own gitlab/github, with necessary comment for other reviewers.
    - Show temperature infomation by serial port as well.
    - Light up LED if temperature sensor not detected (unplugged)

- 3) - Knowledge and application 1: 
    - Make a quick demostrations on the A4 paper to explan how IR remote / DC motor encoder works.

- 4) - Knowledge and application 2: 
    - Read websockets protocol and make a basic websockets shakehands, ping/pong, echo test above the tcp layer on ESP8266 or ESP32.

- 5) - Genral design ability:
    - Here is a ESP32 controlled robot with two stepper motors as wheels, to drive it by WiFi or Bluetooth, 
What you already have: 2 stepper motors, 2 18650 batteries and a dual-battery holder. https://www.jaycar.co.nz/dual-18650-battery-holder/p/PH9207
please check the sourcing website to pickup some main components, evaluate the tradeoff between cost & performance, make a very basic BOM for the components you choose.
